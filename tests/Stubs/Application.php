<?php

namespace Otimizy\IBGE\Tests\Stubs;

class Application
{
    protected $bounds = [];

    public function singleton($abstract, callable $concrete)
    {
        $this->bounds[] = [ $abstract, $concrete([]) ];
    }

    public function getBounds()
    {
        return $this->bounds;
    }
}
