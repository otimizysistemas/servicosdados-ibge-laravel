<?php

namespace Otimizy\Ibge\Tests;

use GuzzleHttp\Client;
use GuzzleHttp\Middleware;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Request;
use Otimizy\Ibge\IbgeAdapter;
use GuzzleHttp\Psr7\Response;
use PHPUnit\Framework\TestCase;
use GuzzleHttp\Handler\MockHandler;

class IbgeAdapterTest extends TestCase
{
    protected $container = [];

    public function testBuscarMunicipiosPorSigla()
    {
        $body = file_get_contents(__DIR__ . '/Storage/MunicipiosRJ.json');

        $adapter = $this->createAdapter(new Response(200, [
            'Content-Type' => 'application/json',
        ], json_encode($body)));

        $response = $adapter->buscarMunicipiosPorSigla('RJ');

        $this->assertIsArray($response);
        $this->assertEquals([ 'status' => 200, 'body' => $body ], $response);
    }

    public function testBuscarMunicipiosPorCodigoIbge()
    {
        $body = file_get_contents(__DIR__ . '/Storage/MunicipiosRJ.json');

        $adapter = $this->createAdapter(new Response(200, [
            'Content-Type' => 'application/json',
        ], json_encode($body)));

        $response = $adapter->buscarMunicipiosPorCodigoIbge(33);

        $this->assertIsArray($response);
        $this->assertEquals([ 'status' => 200, 'body' => $body ], $response);
    }

    protected function createAdapter(Response $response)
    {
        $client = $this->createMockClient(new MockHandler([ $response ]));

        return new IbgeAdapter($client);
    }

    protected function createMockClient(MockHandler $mock)
    {
        $history = Middleware::history($this->container);
        $stack = tap(HandlerStack::create($mock))->push($history);

        return new Client([ 'http_errors' => false, 'handler' => $stack ]);
    }
}
