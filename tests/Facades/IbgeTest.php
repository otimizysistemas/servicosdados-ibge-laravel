<?php

namespace Otimizy\Ibge\Tests\Facades;

use Otimizy\Ibge\Facades\Ibge;
use Orchestra\Testbench\TestCase;
use Otimizy\Ibge\IbgeServiceProvider;

class IbgeTest extends TestCase
{
    public function testBuscarMunicipios()
    {
        $exptectedBody = json_decode(
            file_get_contents(__DIR__ . '/../Storage/MunicipiosRJ.json'),
            true
        );

        Ibge::shouldReceive('buscarMunicipiosPorSigla')
            ->andReturn($exptectedBody);
        $body = Ibge::buscarMunicipiosPorSigla('RJ');

        $this->assertEquals($exptectedBody, $body);
    }

    protected function getPackageProviders($app)
    {
        return [ IbgeServiceProvider::class ];
    }

    protected function getPackageAliases($app)
    {
        return [ 'Ibge' => Ibge::class ];
    }
}
