<?php

namespace Otimizy\Ibge\Tests;

use Otimizy\Ibge\IbgeAdapter;
use PHPUnit\Framework\TestCase;
use Otimizy\Ibge\IbgeServiceProvider;
use Otimizy\Ibge\Tests\Stubs\Application;

class IbgeServiceProviderTest extends TestCase
{
    protected $provider;
    protected $application;

    public function setUp(): void
    {
        parent::setUp();

        $this->application = new Application;
        $this->provider = new IbgeServiceProvider($this->application);
    }

    public function testRegister()
    {
        $response = $this->provider->register();

        $this->assertNull($response);
        $this->assertCount(1, $this->application->getBounds());

        list($abstract, $concrete) = $this->application->getBounds()[0];

        $this->assertEquals('ibge', $abstract);
        $this->assertInstanceOf(IbgeAdapter::class, $concrete);
    }

    public function testProvides()
    {
        $response = $this->provider->provides();

        $this->assertEquals([ 'ibge' ], $response);
    }
}
