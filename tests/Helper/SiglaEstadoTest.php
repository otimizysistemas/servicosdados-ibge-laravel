<?php

namespace Otimizy\Ibge\Tests\Helper;

use Orchestra\Testbench\TestCase;
use Otimizy\Ibge\Helper\SiglaEstadoHelper;

class SiglaEstadoTest extends TestCase
{
    public function testConverterParaIbge()
    {
        $codigo = SiglaEstadoHelper::converterParaIbge('RS');
        $this->assertEquals($codigo, 43);
    }
}
