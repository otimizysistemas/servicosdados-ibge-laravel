[![pipeline status](https://gitlab.com/otimizysistemas/servicosdados-ibge-laravel/badges/master/pipeline.svg)](https://gitlab.com/otimizysistemas/servicosdados-ibge-laravel/commits/master)
[![coverage report](https://gitlab.com/otimizysistemas/servicosdados-ibge-laravel/badges/master/coverage.svg)](https://gitlab.com/otimizysistemas/servicosdados-ibge-laravel/commits/master)


# Serviços Dados IBGE - Laravel

Este é um pacote para realizar a integração de uma aplicação Laravel com a API do IBGE para busca de dados.

## Instalação

Instale o pacote via Composer.

```shell
composer require otimizysistemas/servicosdados-ibge-laravel
```

Caso você não utilize o auto-discovery, registre o provider e o alias em sua aplicação.

```
Otimizy\Ibge\IbgeServiceProvider::class,

...

'Ibge' => Otimizy\Ibge\Facades\Ibge::class,
```


## Métodos disponíveis

Os seguintes métodos estão disponíveis para uso na API.

* `Ibge::buscarMunicipiosPorSigla()`: Realiza a busca dos municípios conforme a sigla informada;
* `Ibge::buscarMunicipiosPorCodigoIbge()`: Realiza a busca dos municípios conforme o código IBGE do estado informado.
