<?php

namespace Otimizy\Ibge;

use GuzzleHttp\Client;
use Otimizy\Ibge\IbgeAdapter;
use Illuminate\Support\ServiceProvider;

class IbgeServiceProvider extends ServiceProvider
{
    protected $defer = true;

    public function register()
    {
        $this->app->singleton('ibge', function ($app) {
            $client = new Client([
                'base_uri' => 'http://servicodados.ibge.gov.br/api/v1/',
                'http_errors' => false,
            ]);

            return new IbgeAdapter($client);
        });
    }

    public function provides()
    {
        return [ 'ibge' ];
    }
}
