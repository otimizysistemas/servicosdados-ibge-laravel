<?php

namespace Otimizy\Ibge;

use GuzzleHttp\Client;
use Otimizy\Ibge\Helper\SiglaEstadoHelper;
use Psr\Http\Message\ResponseInterface;

class IbgeAdapter
{
    protected $client;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    public function buscarMunicipiosPorSigla($siglaEstado)
    {
        $ibgeEstado = SiglaEstadoHelper::converterParaIbge($siglaEstado);
        return $this->buscarMunicipios($ibgeEstado);
    }

    public function buscarMunicipiosPorCodigoIbge($codigo)
    {
        return $this->buscarMunicipios($codigo);
    }

    protected function buscarMunicipios($codigo)
    {
        $response = $this->makeGet('localidades/estados/' . $codigo . '/municipios');

        return $this->parseBody($response);
    }

    protected function makeGet($url)
    {
        return $this->client->get($url);
    }

    protected function parseBody(ResponseInterface $response)
    {
        return [
            'status' => $response->getStatusCode(),
            'body' => json_decode((string) $response->getBody(), true),
        ];
    }
}
