<?php

namespace Otimizy\Ibge\Facades;

use Illuminate\Support\Facades\Facade;

class Ibge extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'ibge';
    }
}
